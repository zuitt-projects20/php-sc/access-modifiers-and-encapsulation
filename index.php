<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S4: Access Modifiers and Inheritance</title>
</head>
<body>
	<h1>S4: Activity</h1>
	<h2>Building</h2>
	<p>The name of the Building is <?= $building->getName();?> </p>
	<p>The <?= $building->getName(); ?> has <?= $building->getFloors(); ?> floors </p>
	<p>The <?= $building->getName(); ?> is located at <?= $building->getAddress(); ?></p>

	<?php $building->setName("Caswyn Complex"); ?>
	<p>The name of the building has been changed to <?= $building->getName(); ?> </p>

	<h1>Condominium</h1>
	<p>The name of the condominium is <?= $condominium->getName();?> </p>
	<p>The <?= $condominium->getName(); ?> has <?= $condominium->getFloors(); ?> floors </p>
	<p>The <?= $condominium->getName(); ?> is located at <?= $condominium->getAddress(); ?></p>

	<?php $condominium->setName("Enzo Tower"); ?>
	<p>The name of the building has been changed to <?= $condominium->getName(); ?> </p>


</body>
</html>